<?php
/**
 * Plugin Name:       WooCommerce Duty Calculator
 * Plugin URI:        https://bitbucket.org/lewisdorigo/woocommerce-duty/
 * Description:       Allows you to add import duty to the price of alcoholic products in WooCommerce.
 * Version:           1.1.1
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */

namespace Dorigo;

class Duty {
    private $post_type = 'woo_country_duty';

    private $bottleVolumeAttr = 'bottle-volume';
    private $abvAttr          = 'alcohol-by-volume';

    private $duty_country;
    private $default_country;

    public function __construct() {
        add_action('init', [$this, 'post_type'], 0);
        add_action('init', [$this, 'post_attributes'], 0);

        add_filter("manage_edit-{$this->post_type}_columns", [$this, 'admin_columns']);
        add_filter("manage_{$this->post_type}_posts_custom_column", [$this, 'column_content'], 10, 2);

        add_action('add_meta_boxes', [$this, 'meta_box']);
        add_action('save_post', [$this, 'save_meta']);

        add_filter('wp_insert_post_data' , [$this, 'post_title'] , 99, 1);

        add_action('post_updated_messages', [$this,'disallow_duplicate_country']);

        if(!is_admin()) {
            add_filter('woocommerce_product_variation_get_price', [$this, 'add_duty'], 10, 2);
            add_filter('woocommerce_product_get_price', [$this,'add_duty'], 10, 2);
        }
    }

    public function post_type() {
        register_post_type($this->post_type, [
    		'label'                 => __('Duty'),
    		'description'           => __('Custom Woocommerce duty'),
    		'labels'                => [
        		'name'                  => __('Duties'),
        		'singular_name'         => __('Duty'),
        		'menu_name'             => __('Duty'),
        		'name_admin_bar'        => __('Post Type'),
        		'archives'              => __('Duty Archives'),
        		'attributes'            => __('Attributes'),
        		'parent_item_colon'     => __('Parent Country:'),
        		'all_items'             => __('All Countries'),
        		'add_new_item'          => __('Add New Country'),
        		'add_new'               => __('Add New'),
        		'new_item'              => __('New Country'),
        		'edit_item'             => __('Edit Country'),
        		'update_item'           => __('Update Country'),
        		'view_item'             => __('View Country'),
        		'view_items'            => __('View Countries'),
        		'search_items'          => __('Search Countries'),
        		'not_found'             => __('Not found'),
        		'not_found_in_trash'    => __('Not found in Trash'),
        		'featured_image'        => __('Featured Image'),
        		'set_featured_image'    => __('Set featured image'),
        		'remove_featured_image' => __('Remove featured image'),
        		'use_featured_image'    => __('Use as featured image'),
        		'insert_into_item'      => __('Insert into country'),
        		'uploaded_to_this_item' => __('Uploaded to this item'),
        		'items_list'            => __('Countries list'),
        		'items_list_navigation' => __('Countries list navigation'),
        		'filter_items_list'     => __('Filter xountries list'),
    		],
    		'supports'              => [null],
    		'taxonomies'            => [],
    		'hierarchical'          => false,
    		'public'                => false,
    		'show_ui'               => true,
    		'show_in_menu'          => true,
    		'menu_position'         => 58,
    		'menu_icon'             => 'dashicons-feedback',
    		'show_in_admin_bar'     => false,
    		'show_in_nav_menus'     => false,
    		'can_export'            => true,
    		'has_archive'           => false,
    		'exclude_from_search'   => true,
    		'publicly_queryable'    => false,
    		'rewrite'               => false,
    		'capability_type'       => 'post',
        ]);
    }

    public function post_attributes() {

        if(wc_attribute_taxonomy_id_by_name($this->bottleVolumeAttr) === 0) {
            wc_create_attribute([
                'slug' => $this->bottleVolumeAttr,
                'name' => 'Bottle Volume',
                'has_archives' => false,
            ]);
        }

        if(wc_attribute_taxonomy_id_by_name($this->abvAttr) === 0) {
            wc_create_attribute([
                'slug' => $this->abvAttr,
                'name' => 'Alcohol by Volume',
                'has_archives' => false,
            ]);
        }

    }

    public function admin_columns($columns) {
        return [
    		'cb'           => '<input type="checkbox" />',
    		'title'        => __('Country Name'),
    		'country_code' => __('Country Code'),
    		'duty'         => __('Duty Price / litre'),
    		'date'         => __('Date'),
		];
    }

    public function column_content($column, $post_id) {
        switch($column) {
            case 'country_code':
                echo get_post_meta($post_id, 'duty_country_code', true);
                break;
            case 'duty':
                echo wc_price(get_post_meta($post_id, 'duty_price', true));
                break;
        }
    }

    private static function get_meta($field = 'duty_price', $post_id = null, $default = false) {
        $post = get_post($post_id);

        $value = get_post_meta($post->ID, $field, true);

        if(!empty($value)) {
            return is_array($value) ? stripslashes_deep($value) : stripslashes(wp_kses_decode_entities($value));
        } else {
            return false;
        }
    }

    public function meta_box() {
        add_meta_box('duty-duty', __('Duty'), [$this,'meta_html'], $this->post_type, 'normal', 'default');
    }

    public function meta_html($post) {
        require_once(__DIR__.'/inc/admin/meta-box.php');
    }

    public function save_meta($post_id) {
        if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { return; }

        if(!isset($_POST['duty_nonce']) || !wp_verify_nonce($_POST['duty_nonce'], '_duty_nonce')) { return; }


        if(isset($_POST['duty_country_code'])) {
            update_post_meta($post_id, 'duty_country_code', esc_attr($_POST['duty_country']));
        }

    	if(isset($_POST['duty_price'])) {
    		update_post_meta($post_id, 'duty_price', esc_attr($_POST['duty_price']));
        }

        if(isset( $_POST['duty_country'])) {
    		update_post_meta($post_id, 'duty_country', esc_attr( $_POST['duty_country']));
        }
    }

    public function post_title($data) {
        if($data['post_type'] === $this->post_type && isset($_POST['duty_country'])) {
            $countries_obj = new \WC_Countries();
            $countries     = $countries_obj->__get('countries');

            $country_code = $_POST['duty_country'];

            $data['post_title'] = isset($countries[$country_code]) ? $countries[$country_code] : 'Unknown Country';
        }

        return $data;
    }

    public function disallow_duplicate_country($messages) {
        global $post;
        global $wpdb;

        $post_id = $post->ID;
        $country_code = self::get_meta('duty_country_code', $post_id);

        $results = get_posts([
            'post_type' => $this->post_type,
            'meta_query' => [
                [
                    'key' => 'duty_country_code',
                    'value' => $country_code,
                    'compare' => '=',
                ],
            ],
            'post__not_in' => [$post_id],
        ]);

        if(!empty($results)) {
            $result = $results[0];

            $error_message = 'A duty has already been entered for <a href="'.get_edit_post_link($result->ID).'" target="_blank">this country</a>. This country’s duty has not been published.';
            add_settings_error('post_has_links','',$error_message,'error');
            settings_errors('post_has_links');

            $post->post_status = 'draft';
            wp_update_post($post);

            return;
        }

        return $messages;
    }

    public function get_countries() {
        global $woocommerce;

        if(isset($woocommerce->customer)) {
            $this->duty_country = $woocommerce->customer->get_shipping_country();
        } else {
            $this->duty_country = (isset($_COOKIE['country']) ? $_COOKIE['country'] : null);
        }

        $default_location = wc_get_base_location();

        $this->default_country  = $default_location['country'] ? apply_filters('woocommerce_countries_base_country', $default_location['country']) : null;

        $this->duty_country = strtoupper($this->duty_country);
        $this->default_country = strtoupper($this->default_country);
    }

    public function add_duty($price, $product) {
        global $woocommerce;

        if(!$this->duty_country || $this->default_country) {
            $this->get_countries();
        }

        if(!$this->duty_country || $this->duty_country === $this->default_country) { return $price; }

        $duty = get_posts([
            'post_type'        => 'woo_country_duty',
            'posts_per_page'   => 1,
            'meta_key'         => 'duty_country_code',
            'post_status'      => 'publish',
            'order'            => 'ASC',
            'fields'          => 'ids',
            'meta_query'       => [
                [
                    'key'     => 'duty_country_code',
                    'value'   => $this->duty_country,
                    'compare' => '=',
                ],
            ],
        ]);

        if(!empty($duty)) {
            $product_id = $product->Get_id();

            $duty_percentage = (float) get_post_meta(current($duty), 'duty_price', true);

            $alcohol_vol = wc_get_product_terms($product_id, 'pa_'.$this->bottleVolumeAttr);
            $alcohol_abv = wc_get_product_terms($product_id, 'pa_'.$this->abvAttr);


            if(get_class($product) === 'WC_Product_Variation' && $product->get_parent_id() &&  ($alcohol_vol === 0 || $alcohol_abv === 0)) {
                $parent_id = $product->get_parent_id();

                $alcohol_vol = $alcohol_vol ?: wc_get_product_terms($parent_id, 'pa_'.$this->bottleVolumeAttr);
                $alcohol_abv = $alcohol_abv ?: wc_get_product_terms($parent_id, 'pa_'.$this->abvAttr);
            }

            $alcohol_vol = !empty($alcohol_vol) ? (float) $alcohol_vol[0]->name : 0;
            $alcohol_abv = !empty($alcohol_abv) ? (float) $alcohol_abv[0]->name : 0;

            $duty_calc       = $duty_percentage * ($alcohol_vol / 100) * ($alcohol_abv / 100);

            return $price + $duty_calc;
        }

        return $price;
    }
}

new Duty();
