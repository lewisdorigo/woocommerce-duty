<?php namespace Dorigo;

    global $woocommerce;
    wp_nonce_field('_duty_nonce', 'duty_nonce');
?>
<div class="acf-fields">
    <div class="acf-field acf-field-select" style="width: 50%" data-width="50%">
        <div class="acf-label">
            <label for="duty_country"><?php _e('Country'); ?></label>
        </div>

        <div class="acf-input">
            <?php
                $countries_obj = new \WC_Countries();
                $countries     = $countries_obj->__get('countries');
            ?>
            <select name="duty_country" id="duty_country" required>
            <?php foreach ($countries as $country_code => $country): ?>
                  <option value="<?= $country_code ?>" <?= (Duty::get_meta('duty_country') == $country_code) ? 'selected' : ''; ?> ><?= $country ?></option>
            <?php endforeach; ?>
            </select>
        </div>
    </div>


    <div class="acf-field acf-field-text" style="width: 50%" data-width="50%">
        <div class="acf-label">
            <label for="duty_country_code"><?php _e('Country Code'); ?></label>
        </div>

        <div class="acf-input">
        	<input type="text" name="duty_country_code" id="duty_country_code" value="<?= Duty::get_meta('duty_country_code'); ?>" readonly><br>
        </div>
    </div>

    <div class="acf-field acf-field-text">
        <div class="acf-label">
        	<label for="duty_price">
        	    <?php _e( 'Duty Price'); ?> (<?= get_woocommerce_currency_symbol(); ?>)<br>
                <small>This is the price per litre of alcohol.</small>
            </label>
        </div>

        <div class="acf-input">
            <input type="number" min="1" step=".01" name="duty_price" id="duty_price" value="<?= Duty::get_meta('duty_price'); ?>">
        </div>
    </div>
</div>

<script>
    var countries = <?= json_encode($countries); ?>;

    jQuery('#duty_country').on('change', function() {
        jQuery('#duty_country_code').val($(this).val());
    });
</script>
